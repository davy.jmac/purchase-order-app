const express = require('express');
const router = express.Router();

// add
router.post('/purchase-order', (req, res) => {
    console.log("Add for ID: ", req.body.id)

    var fs = require('fs');
    fs.writeFile(`src/main/resources/static/backend/data/purchaseOrders/${req.body.id}.json`, JSON.stringify(req.body), (err) => err && console.error(err));

    res.send("success");// this gets executed when catalog visit http://localhost:3000/purchase-orders
});

// find by id 
router.get('/purchase-orders', (req, res) => {

    var fs = require('fs');

    fs.readdir(`src/main/resources/static/backend/data/purchaseOrders`, (err, files) => {
        res.send(files);
    })
});


router.get('/purchase-order', (req, res) => {

    var fs = require('fs');

    fs.readFile(`src/main/resources/static/backend/data/purchaseOrders/${req.query.id}`, (err, file) => {
        console.log(JSON.parse(file))
        if (err) {
            console.error('Error while reading the file:', err)
            return
        }
        try {
            const data = JSON.parse(file);
            res.send(data);

        } catch (err) {
            console.error('Error while parsing JSON data:', err);
        }
    })

});

router.get('/purchase-order/delete', (req, res) => {

    var fs = require('fs');

    fs.readFile(`src/main/resources/static/backend/data/purchaseOrders/${req.query.id}`, (err, file) => {
        console.log(JSON.parse(file))
        if (err) {
            console.error('Error while reading the file:', err)
            return
        }
        try {
            const data = JSON.parse(file);
            res.send(data);

        } catch (err) {
            console.error('Error while parsing JSON data:', err);
        }
    })

});
module.exports = router;