const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.send('This is catalog route');// this gets executed when catalog visit http://localhost:3000/catalog
});


// find all parts
router.get('/parts-catalog', (req, res) => {

    var fs = require('fs');
    console.log(req)

    fs.readFile(`src/main/resources/static/backend/data/parts/parts.json`, (err, file) => {
        if (typeof file !== 'undefined') {
            console.log(JSON.parse(file))
            if (err) {
                console.error('Error while reading the file:', err)
                return
            }
            try {
                const data = JSON.parse(file);
                res.send(data);

            } catch (err) {
                console.error('Error while parsing JSON data:', err);
            } 
        }
    })
});

module.exports = router;