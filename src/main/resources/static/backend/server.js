// Example using Express.js
const express = require('express');
const app = express();

// Example specifying the port and starting the server
const port = process.env.PORT || 3000; // You can use environment variables for port configuration
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

var cors = require("cors");
app.use(cors());
app.use(express.json());

// Example defining a route in Express
app.get('/', (req, res) => {
    res.send('<h1>Bakcend of purchase order app</h1>');
});

const catalogRoute = require('./routes/parts');
app.use('/parts', catalogRoute)

const purchaseOrdersRoute = require('./routes/purchaseOrders');
app.use('/purchase-orders', purchaseOrdersRoute)
