import './styles.css';
import './views/init-view/init-view.js';
import './views/app-header/menu/menu-view.js';
import './views/app-header/app-header-view.js';
import './router-outlet.js';
import './views/catalog-view/catalog-view.js';
import './views/purchase-orders-view/purchase-orders-view.js';
import './views/create-purchase-order-view/create-purchase-order-view.js';

