export const SET_VIEW = 'SET_VIEW';

export const navigate = (view) => (dispatch) => {
  dispatch(updateView(view));
  window.scroll(0, 0);
};

export const updateView = (view) => {
  return {
    type: SET_VIEW,
    view
  };
};
