import moment from 'moment';
import {
  fetchAddPuchaseOrder,
  fetchPuchaseOrders,
  fetchPuchaseOrderById,
  fetchParts
} from '../services/app-api.js';
import { store } from '../store.js';

export const SET_PURCAHSE_ORDERS = "SET_PURCAHSE_ORDERS";
export const SET_PARTS = "SET_PARTS";

export const addPurchaseOrder = (purchaseOrder) => (dispatch, getState) => {  
  return fetchAddPuchaseOrder(purchaseOrder)
  .then((response) => {
    if (response.ok) {
      return response.statusText;
    } else {
      console.log("Add purchase order failed 1");
      throw Error(response.statusText);
    }
  })
  .then((statusText) => {
    console.log("data: ", statusText)
    dispatch(getPurchaseOrders());
  })
  .catch((error) => {
    console.log("Add purchase order failed 2");
    console.error(error);
  });
};

export const  getPurchaseOrders = () => async (dispatch, getState) => {  
  return fetchPuchaseOrders()
  .then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      console.log("Add purchase order failed 1");
      throw Error(response.statusText);
    }
  })
  .then((files) => {
    console.log("data: ", files)

    files.forEach((file) => {
       dispatch(getPurchaseOrderById(file));
    })
  })
  .catch((error) => {
    console.log("Add purchase order failed 2");
    console.error(error);
  });
};

export const getPurchaseOrderById = (purchaseOrderId) => (dispatch, getState) => {  
  let files = [];

  return fetchPuchaseOrderById(purchaseOrderId)
  .then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      console.log("Add purchase order failed 1");
      throw Error(response.statusText);
    }
  })
  .then((data) => {
    dispatch(setPurchaseOrder(data));
  })
  .catch((error) => {
    console.log("Add purchase order failed 2");
    console.error(error);
  });
};

export const getParts = () => (dispatch, getState) => {  

  return fetchParts()
  .then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      console.log("Add purchase order failed 1");
      throw Error(response.statusText);
    }
  })
  .then((data) => {
    dispatch(setParts(data));
  })
  .catch((error) => {
    console.log("Add purchase order failed 2");
    console.error(error);
  });
};

export const setParts = (payload) => {
  return {
    type: SET_PARTS,
    payload,
  };
};

export const setPurchaseOrder = (payload) => {
  return {
    type: SET_PURCAHSE_ORDERS,
    payload,
  };
};
