import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { lazyReducerEnhancer } from 'pwa-helpers/lazy-reducer-enhancer.js';
import { app } from './reducers/app-reducer.js';
import  thunk  from 'redux-thunk';

export const store = createStore(
  state => state,
  composeWithDevTools(lazyReducerEnhancer(combineReducers), applyMiddleware(thunk))
);

store.addReducers({
  app
});
