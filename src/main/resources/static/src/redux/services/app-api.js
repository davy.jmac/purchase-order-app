import { store } from "../store.js";

const URL = "http://localhost:3000";

export const fetchAddPuchaseOrder = (purchaseOrder) => {
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  
  const request = new Request(
  `${URL}/purchase-orders/purchase-order`,
    {
      method: "POST",
      headers: headers,
      body: JSON.stringify(purchaseOrder),
      mode: "cors",
      cache: "default",
    }
  );
  return fetch(request);
};

export const fetchPuchaseOrders = () => {
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  
  const request = new Request(
  `${URL}/purchase-orders/purchase-orders`,
  {
    method: "GET",
    headers: headers,
    mode: "cors",
    cache: "default",
  }
  );
  return fetch(request);
};


export const fetchPuchaseOrderById = (purchaseOrder) => {
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  
  const request = new Request(
  `${URL}/purchase-orders/purchase-order?id=${purchaseOrder}`,
  {
    method: "GET",
    headers: headers,
    mode: "cors",
    cache: "default",
  }
  );
  return fetch(request);
};

export const fetchParts = () => {
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  
  const request = new Request(
  `${URL}/parts/parts-catalog`,
  {
    method: "GET",
    headers: headers,
    mode: "cors",
    cache: "default",
  }
  );
  return fetch(request);
};