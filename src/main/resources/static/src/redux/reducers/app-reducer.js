import { SET_VIEW } from "../actions/nav-actions.js";
import { SET_PURCAHSE_ORDERS, SET_PARTS } from "../actions/app-actions.js";
import { store } from '../store.js';

const INITIAL_STATE = {
  view: "catalog",
  purchaseOrders: [],
  parts: []
};

export const app = (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case SET_VIEW:
      return {
        ...state,
        view: action.view,
      };
    case SET_PURCAHSE_ORDERS:
      
      let array = state.purchaseOrders;
      if (state.purchaseOrders.filter((po) => po.id === action.payload.id).length === 0) {
        array.push(action.payload)
      }

      return {
        ...state,
        purchaseOrders: array,
      };

      case SET_PARTS:

      return {
        ...state,
        parts: action.payload,
      };
    default:
      return state;
  }
};
