import { installRouter } from "pwa-helpers/router.js";
import { LitElement, html } from "lit";
import { BaseView } from './views/base-view.js';
import { connect } from 'pwa-helpers';
import { store } from './redux/store.js';

class RouterOutlet extends connect(store)(LitElement, BaseView) {
  
  render() {
    switch (this.view) {
      case "catalog":
        return html `<catalog-view></catalog-view>`;
      case "create-purchase-order":
        return html `<create-purchase-order-view></create-purchase-order-view>`;
      case "purchase-orders":
        return html `<purchase-orders-view></purchase-orders-view>`;
      default:
        return html `<page-not-found-view></page-not-found-view>`;
    }
  }

  static get properties() {
    return {
      view: { type: String },
      flows: { type: Array },
      productName: { type: String }
    };
  }

  constructor() {
    super();
    this.productName = null;
    this.previousView = null;
    // this.view = "catalog";
    this.flows = null;

    installRouter((location) => {
      //console.log("Router = ", location);
      //store.dispatch(navigate(location.pathname));
    });
  }

  stateChanged(state) {
    this.view = state.app.view;
  }
}

window.customElements.define("router-outlet", RouterOutlet);
