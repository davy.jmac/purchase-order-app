import { LitElement } from "lit";
import { purchaseOrdersViewTemplate } from "./purchase-orders-view.template.js";
import { getPurchaseOrders, addPurchaseOrder } from "../../redux/actions/app-actions.js";
import { store } from '../../redux/store.js';
import { connect } from 'pwa-helpers';
import { BaseView } from '../base-view.js';

const SMS_VALIDATION = /\+?1?\s*\(?-*\.*(\d{3})\)?\.*-*\s*(\d{3})\.*-*\s*(\d{4})$/;
const EMAIL_VALIDATION = /^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;


class PurchaseOrdersView extends connect(store)(LitElement,BaseView) {
  render() {
    return purchaseOrdersViewTemplate(this);
  }

  static get properties() {
    return {
      id: { type: String },
      purchaseOrders: { type: Array },
      selectedPurchaseOrder: { type: Object },
      selectedPurchaseOrderIndex: { type: Number },
      selectedPart: { type: Object },
      selectedPartIndex: { type: Object },
      showEditPart: { type: Boolean },
      showEditPurchaseOrder: { type: Boolean },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.id = "header-view";
    this.purchaseOrders = [];

    this.selectedPurchaseOrder = null;
    this.selectedPurchaseOrderIndex = null;

    this.selectedPart = null;
    this.selectedPartIndex = null;

    this.showEditPart = false;
    this.showEditPurchaseOrder = false;

    this.error = "";

    store.dispatch(getPurchaseOrders());
    this.addEventListener("changed-value", this.valueUpdated);
  }

  stateChanged(state) {

    if (state.app.purchaseOrders) {
      this.purchaseOrders = [...state.app.purchaseOrders];
    }

  }

  valueUpdated(e) {
    if (!this.validate(e)) {
      return;
    }

    switch (e.detail.id) {
      case "name":
        this.selectedPurchaseOrder.customer.name = e.detail.value;
        break;
      case "email":
        this.selectedPurchaseOrder.customer.email = e.detail.value;
        break;
      case "address":
        this.selectedPurchaseOrder.customer.address = e.detail.value;
        break;
      case "postalCode":
        this.selectedPurchaseOrder.customer.postalCode = e.detail.value;
        break;
      case "phone":
        this.selectedPurchaseOrder.customer.phone = e.detail.value;
        break;
      case "company":
        this.selectedPurchaseOrder.customer.company = e.detail.value;
        break;
      case "shipping":
        if (e.detail.value === "true") {
          this.selectedPurchaseOrder.customer.shipping = true;
        } else {
          this.selectedPurchaseOrder.customer.shipping = false;
        }
        break;
      case "shippingAddress":
        this.selectedPurchaseOrder.customer.shippingAddress = e.detail.value;
        break;
      case "shippingPostalCode":
        this.selectedPurchaseOrder.customer.shippingPostalCode = e.detail.value;
        break;
      case "pickup":
        if (e.detail.value === "true") {
          this.selectedPurchaseOrder.customer.pickup = true;
        } else {
          this.selectedPurchaseOrder.customer.pickup = false;
        }
        break;
      case "partNumber":

        if (this.parts.filter((part) => part.partNumber === Number(e.detail.value)).length > 0) {
          let part = this.parts.filter((part) => part.partNumber === Number(e.detail.value))[0]

          this.selectedPart.partNumber = part.partNumber;
          this.selectedPart.partName = part.partName;
          this.selectedPart.partPrice = part.partPrice;
          this.selectedPart.partTotal = this.selectedPart.partQuantity * this.selectedPart.partPrice;
        }

        break;
      case "partPrice":
        break;
      case "partName":
        break;
      case "partQuantity":
        console.log("JAMES")
        this.selectedPart.partQuantity = e.detail.value;
        this.selectedPart.partTotal = this.selectedPart.partQuantity * this.selectedPart.partPrice;
        break;
      default: 
        console.log("Invalid id")
    }
    this.purchaseOrders[this.selectedPurchaseOrderIndex] =  this.selectedPurchaseOrder;
    this.purchaseOrders = [...this.purchaseOrders]
    // this.selectedPurchaseOrder = {...this.selectedPurchaseOrder}
  }



  validate(e) {

    if (e.detail.value !== "") {
      this.invalid = "";
      this.error = "";
    } else {
      console.log("entered")
      this.invalid = e.detail.id;
      this.error = "Field required";
      return false;
    }

    switch (e.detail.id) {
      case "email":
        if (EMAIL_VALIDATION.test(e.detail.value)) {
          this.invalid = "";
        } else {
          this.invalid = e.detail.id;
          this.error = "Invalid email";
          return false;
        }
        break;
      case "phone":
        if (SMS_VALIDATION.test(e.detail.value)) {
          this.invalid = "";
        } else {
          this.invalid = e.detail.id;
          this.error = "Invalid phone number";
          return false;
        }
        break;
      default: 
        break;
    }
    return true;
  }

  /** */
  editPurchaseOrder(e, index) {
    this.showEditPart = false;
    this.showEditPurchaseOrder = true;

    this.selectedPurchaseOrderIndex = index;

    this.selectedPurchaseOrder = this.purchaseOrders[index];
  }

  deletePurchaseOrder(e, index) {
    this.purchaseOrders.splice(index, 1);
    this.purchaseOrders = [...this.purchaseOrders]
  }

  editPart(e, index, partIndex) {
    this.showEditPart = true;
    this.showEditPurchaseOrder = false;

    this.selectedPurchaseOrderIndex = index;
    this.selectedPartIndex = partIndex;

    this.selectedPurchaseOrder = this.purchaseOrders[index];
    this.selectedPart = this.purchaseOrders[index].parts[partIndex];
  }

  savePart() {
    this.selectedPurchaseOrder.parts[this.selectedPartIndex];
    store.dispatch(addPurchaseOrder(this.selectedPurchaseOrder));

    this.showEditPart = false;
    this.selectedPartIndex = null;

  }

  savePurchaseOrder() {
    store.dispatch(addPurchaseOrder(this.selectedPurchaseOrder));

    this.showEditPurchaseOrder = false;
    this.selectedPurchaseOrderIndex = null;
  }

}

customElements.define("purchase-orders-view", PurchaseOrdersView);