import { html } from 'lit'; 
import { purchaseOrdersViewStyles } from './purchase-orders-view.styles.js';
import { sharedStyles } from '../shared/shared-styles.js';
import { pencilIcon, blackDeleteIcon } from "../../icons.js"

export const purchaseOrdersViewTemplate = (context) => {
  return html`
    ${ sharedStyles }
    ${ purchaseOrdersViewStyles }
    <div class="wrapper">
      <div class="layout">
        <div class="header-container">
          Purchase Orders  
        </div>
          ${context.purchaseOrders ? context.purchaseOrders.map((purchaseOrder, index) => {
            return html`
              <div class="purchase-orders-container">
                <div class="purchase-order-customer-container">

                  <div class="customer-header-container">
                    <div class="customer-header">
                      Customer Name
                    </div>
                    <div class="customer-header">
                      Customer Company
                    </div>
                    <div class="customer-header">
                      Customer Email
                    </div>
                    <div class="customer-header">
                      Shipping / Pickup
                    </div>
                  </div>

                  <div class="customer-body-row">
                    <div class="customer-row">
                      ${purchaseOrder.customer.name}
                    </div>
                    <div class="customer-row">
                      ${purchaseOrder.customer.company}
                    </div>
                    <div class="customer-row">
                      ${purchaseOrder.customer.email}
                    </div>
                    ${purchaseOrder.customer.shipping ? 
                      html`
                        <div class="customer-row">
                          Shipping
                        </div>
                    `
                    :
                      html`
                        <div class="customer-row">
                          Pickup
                        </div>
                      `
                    }
                    <div class="pencil-icon"
                      @click=${(e) => context.editPurchaseOrder(e, index)}
                    > 
                      ${pencilIcon}
                    </div>

                    <div class="pencil-icon"
                      @click=${(e) => context.deletePurchaseOrder(e, index)}
                    > 
                      ${blackDeleteIcon}
                    </div>
                  </div>
                </div>

                ${context.selectedPurchaseOrder && context.selectedPurchaseOrderIndex === index && context.showEditPurchaseOrder  ? 
                  html`
                    <div>
                      ${buildCustomerInputs(context.selectedPurchaseOrder, context)}
                    </div>
                    <div class="error-container">
                      ${context.error}
                    </div>
                  `
                :
                  html``
                }
                
                <div class="parts-header-container">
                  <div class="part-header">
                    Part Name
                  </div>
                  <div class="part-header">
                    Part Quantity
                  </div>
                  <div class="part-header">
                    Part Total
                  </div>
                </div>
                ${purchaseOrder.parts.length > 0 ? purchaseOrder.parts.map((part, partIndex) => {
                    return html`
                      <div class="part-row-container">
                        <div class="part-row">
                          ${part.partName}
                        </div>
                        <div class="part-row">
                          ${part.partQuantity}
                        </div>
                        <div class="part-row">
                          ${part.partTotal}
                        </div>
                        <div class="pencil-icon"
                          @click=${(e) => context.editPart(e, index, partIndex)}
                        > 
                          ${pencilIcon}
                        </div>
                      <div>
                    `
                  })
                : 
                  html``
                }
              <div>
              <div class="edit-part-container">
                ${context.selectedPart && context.selectedPurchaseOrderIndex === index && context.showEditPart ? 
                  html`
                    <div>
                      ${buildPurchaseOrderInputs(context.selectedPart, context)}
                    </div>
                    <div class="error-container">
                      ${context.error}
                    </div>
                  `
                :
                  html``
                }
              </div>
            `})
          : 
            html``}
      </div>
    </div>
  `
};

function buildCustomerInputs(selectedPurchaseOrder, context) {
  return html`
    <div class="inputs-container">
      <div class="left-inputs-container">
        <div class="component-container">
          <input-string
            id="name"
            name=Name
            .value=${selectedPurchaseOrder.customer.name}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="address"
            name="Address"
            .value=${selectedPurchaseOrder.customer.address}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="postalCode"
            name="Postal Code"
            .value=${selectedPurchaseOrder.customer.postalCode}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-radio-array
            id="pickup"
            name="Pick Up"
            .options=${[{"optionCode" : "true", "description" : "Yes"}, {"optionCode" : "false", "description" : "No"}]}
            ?required=${true}
            .value=${String(selectedPurchaseOrder.customer.pickup)}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-dropdown-")) : ""}
          >
          </input-radio-array>
        </div>
      </div>

      <div class="middle-inputs-container">
        <div class="component-container">
          <input-string
            id="email"
            name=Email
            .value=${selectedPurchaseOrder.customer.email}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="shippingPostalCode"
            name="Shipping Postal Code"
            .value=${selectedPurchaseOrder.customer.shippingPostalCode}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-radio-array
            id="shipping"
            name="Shipping"
            .options=${[{"optionCode" : "true", "description" : "Yes"}, {"optionCode" : "false", "description" : "No"}]}
            ?required=${true}
            componentWidth=175
            .value=${String(selectedPurchaseOrder.customer.shipping)}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-dropdown-")) : ""}
          >
          </input-radio-array>
        </div>
      </div>
        
      <div class="right-inputs-container">
        <div class="component-container">
          <input-string
            id="phone"
            name="Phone"
            .value=${selectedPurchaseOrder.customer.phone}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="shippingAddress"
            name="Shipping Address"
            .value=${selectedPurchaseOrder.customer.shippingAddress}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="company"
            name="Company"
            .value=${selectedPurchaseOrder.customer.company}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>


        <div class="component-container">
          <button
            @click=${(e) => context.savePurchaseOrder(e)}
          >
            Save
          </button>
        </div>

      </div>
    </div>
  `;
}


function buildPurchaseOrderInputs(selectedPart, context) {
  return html`
    <div class="inputs-container">
      <div class="left-inputs-container">
      <div class="component-container">
          <input-dropdown
           id="partNumber"
           name="Part Number"
           .options=${[{"optionCode" : "1", "description" : "1"}, {"optionCode" : "2", "description" : "2"}, {"optionCode" : "3", "description" : "3"}, {"optionCode" : "4", "description" : "4"}, {"optionCode" : "5", "description" : "5"}, {"optionCode" : "6", "description" : "6"}, {"optionCode" : "7", "description" : "7"}, {"optionCode" : "8", "description" : "8"}]}
           .value=${selectedPart.partNumber}
           ?required=${true}
           componentWidth=175
           .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-droddown-")) : ""}
          >
          </input-dropdown>
        </div>

        <div class="component-container">
          <input-string
            id="partPrice"
            name="Part Price"
            .value=${selectedPart.partPrice}
            ?required=${true}
            componentWidth=175
            ?readOnly=${true}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>
      </div>

      <div class="middle-inputs-container">
        <div class="component-container">
          <input-string
            id="partName"
            name="Part Name"
            .value=${selectedPart.partName}
            ?required=${true}
            componentWidth=175
            ?readOnly=${true}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="partTotal"
            name="Part Total"
            .value=${selectedPart.partTotal}
            ?required=${true}
            componentWidth=175
            ?readOnly=${true}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>
      </div>

      <div class="right-inputs-container">
        <div class="component-container">
          <input-dropdown
            id="partQuantity"
            name="Part Quantity"
            .options=${[{"optionCode" : "1", "description" : "1"}, {"optionCode" : "2", "description" : "2"}, {"optionCode" : "3", "description" : "3"}, {"optionCode" : "4", "description" : "4"}, {"optionCode" : "5", "description" : "5"}, {"optionCode" : "6", "description" : "6"}, {"optionCode" : "7", "description" : "7"}, {"optionCode" : "8", "description" : "8"}, {"optionCode" : "9", "description" : "9"}, {"optionCode" : "10", "description" : "10"}, {"optionCode" : "11", "description" : "11"}, {"optionCode" : "12", "description" : "12"}, {"optionCode" : "13", "description" : "13"}, {"optionCode" : "14", "description" : "14"}, {"optionCode" : "15", "description" : "15"}]}
            .value=${selectedPart.partQuantity}
            ?required=${true}
            componentWidth=175
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-dropdown>
        </div>

        <div class="component-container">
          <button
            @click=${(e) => context.savePart(e)}
          >
            Save
          </button>
        </div>
      </div>
  `;
}