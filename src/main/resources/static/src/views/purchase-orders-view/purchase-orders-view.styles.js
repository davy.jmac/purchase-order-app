import { html } from "lit";

export const purchaseOrdersViewStyles = html`
  <style>
    .purchase-orders-container {
      padding-bottom: 75px;
    }

    .purchase-order-customer-container {
      display: flex;
      flex-direction: column;
    }

    .parts-header-container {
      display: flex;
      flex-direction: row;
      width: 100%;
    }
    
    .customer-header-container {
      display: flex;
      flex-direction: row;
      width: 100%;
    }

    .parts-body-row {
      display: flex;
      flex-direction: row;
      width: 100%;
    }
    
    .customer-body-row {
      display: flex;
      flex-direction: row;
      width: 100%;
    }

    .part-row-container {
      display: flex;
      flex-direction: row;
    }

    .customer-header {
      display: flex;
      width: 25%;
      border: 2px solid var(--white-gray);
      border-bottom: none;
      justify-content: center;
      background-color: var(--white-gray)
    }

    .customer-row {
      display: flex;
      width: 25%;
      border: 2px solid var(--white-gray);
      border-top: none;
      justify-content: center;
      text-align: center;
      padding: 10px;
    } 

    .part-header {
      display: flex;
      width: 33.3%;
      border: 2px solid var(--white-gray);
      border-bottom: none;
      border-top: none;
      justify-content: center;
      background-color: var(--white-gray)
    }

    .part-row {
      display: flex;
      width: 33.3%;
      border: 2px solid var(--white-gray);
      border-top: none;
      justify-content: center;
      text-align: center;
      padding: 10px;
    }

    .header-container {
      font-size: 30px;
      padding: 30px;
    }

    .pencil-icon {
      display: flex;
      justify-content: center;
      align-items: center;
      border: 2px solid var(--white-gray);
      width: 5%;
    }

    .left-inputs-container {
      display: flex;
      flex-direction: column;
      padding-right: 30px;
      padding-left: 30px;
      width: 20%
    }

    .middle-inputs-container {
      display: flex;
      flex-direction: column;
      padding-right: 30px;
      width: 20%
    } 

    .right-inputs-container {
      display: flex;
      flex-direction: column;
      width: 20%;
    } 

    .inputs-container {
      display: flex;
      flex-direction: row;
      padding-top: 30px;
      justify-content: center;
    }

    .error-container {
      padding-left: 720px;
    }
  </style>
`;