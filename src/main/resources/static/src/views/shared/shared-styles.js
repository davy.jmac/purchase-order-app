import { html } from 'lit'; 

export const sharedStyles = html`
<style>
    /* Component styles */
    
    .layout {
      background-color: var(--white);
      height: 100%;
    }

    label, div, body {
      font-family: 'Open Sans', sans-serif;
    }

    .component-label {
      font-size: 14px;
      color: black;
      white-space: nowrap;
      font-family: 'Open Sans', sans-serif;
    }

    .asterisk {
      display: inline;
      color: red;
    }

    .orientation-horizontal {
      display: flex;
      flex-direction: row;
    }
    
    .orientation-vertical {
      display: flex;
      flex-direction: column;
    }

    button {
      min-width: 116px;
      height: 40.8px;
      text-align: center;
      padding: 12px 32px;
      border-radius: 4px;
      border-width: 0px; 
    }

    button.dark {
        background-color: #5465BB;
        color: var(--white);
        box-shadow: 0px 2px 6px #919191;
    }  
    
    button.dark:hover:enabled {
        background-color: #4A90E2;
        cursor: pointer;
    }  

    button.dark:disabled {
        color: var(--white-gray);
        cursor: auto;
    }  

    button.light {
      color: var(--dark-gray);
      background-color: var(--white-gray);
      border: solid;
      border-width: thin;
      border-color: var(--white-gray);
      box-shadow: 0px 2px 6px #919191;
      cursor: pointer;
    }

    button.light:hover {
      background-color: var(--white-gray);
    } 
    
    button.red {
      background-color: red;
      color: var(--white);
      box-shadow: 0px 2px 6px #919191;
      cursor: pointer;
    }

    button.red:hover {
      background-color: red;
    }

    .array-input-label {
        font-size: 12px;
        color: black;
        padding-right: 15px;
        word-break: break-all;
    }

    .single-input-container {
      height: 35px;
      display: flex;
      flex-direction: row;
      position: relative;
    }

    .single-input-container input {
      padding-top: 10px;
      min-height: 26px;
      outline: none;
      border: none;
      /* replace with a gray */
      border-bottom: 1.5px solid var(--black);
      position: relative;
      width: 100%;
    }

    .single-input-container input:focus {
      border-bottom: solid 1.5px #4A90E2;
    }

    .jumping-label {
      align-items: bottom;
      pointer-events: none;
      position: absolute;
      transform-origin: 0 50%;
      transition: transform 200ms, color 200ms;
      top: 8px;
      font-family: 'Open Sans', sans-serif;
    }

    .input:focus ~ .cut,
    .input:not(:placeholder-shown) ~ .cut {
      transform: translateY(8px);
      text-overflow: ellipsis;
    }

    .input:focus ~ .placeholder,
    .input:not(:placeholder-shown) ~ .placeholder {
      transform: translateY(-17px) translateX(10px) scale(0.75);
      left: -10px;
    }

    .cut {
      position: absolute;
      top: -20px;
      transform: translateY(0);
      transition: transform 200ms;
    }

    .error-container {
      color: red;
      font-size: 13px;
      min-height: 18px;
      padding-top: 2px;
      padding-left: 30px;
    }
    
    .pointer {
      cursor: pointer; 
    }

    .i-beam {
      cursor: auto; 
    }

    select option.custom-pointer {
      cursor: pointer;
    }

    .email {
      overflow-x: clip;
      overflow-y: clip;
    }

   .patient-demographics-header {
      font-family: 'Open Sans', sans-serif;
      font-weight: 500;
      font-size: 25px;
      line-height: 23px;
      margin-bottom: 20px;
      padding-top: 30px;
      padding-left: 30px;
      padding-bottom: 20px;
    }

    .names {
      width: 24%;
    }

    .sub-pannel {
      display: flex;
      flex-direction: column;
      padding-bottom: 20px;
      padding-right: 10px;
    }

    .data {
      overflow-wrap: break-word;
      padding-left: 5px;
    }

    .row {
      display: flex;
      flex-direction: row;
      outline: 2px solid var(--white-gray);
    }

    .patient-demographics-container {
      display: flex;
      flex-direction: column;
      font-size: 12px;
    }

    .patient-demographics-table-container {
      display: flex;
      flex-direction: row;
      /* width: 100%; */
      height: 100%;
      padding-left: 30px;
      padding-bottom: 10px;
    }

    .patient-demographics-table {
      display: flex;
      flex-direction: column;
      width: 80%;
    }

    .patient-demographics-table-row {
      display: flex;
      flex-direction: row;
    }
    
    .previous-referral-header, .create-referral-header, .workflow-history-header {
        font-family: 'Open Sans', sans-serif;
        font-weight: 500;
        font-size: 25px;
        line-height: 23px;
        margin-bottom: 30px;
        padding-top: 30px;
    }

    input:disabled {
      background-color: var(--white-gray);
    }

    .simple-table {
      width: 100%;
      border-collapse: collapse;
      border-spacing: 0;
      font-size: 14px;
    }

    .simple-table-header-sticky {
      position: sticky;
      top: 0;
    }

    .simple-table-row {
      text-align: left;
      padding-left: 5px;
      border-right: 1px solid var(--light-gray);
      border-left: 1px solid var(--light-gray);
      border-bottom: 1px solid var(--light-gray);
    }

    .simple-table-cell {
      text-align: center;
      vertical-align: middle;
      padding: 5px;
    }

    .simple-table .scroll {
      table-layout: fixed;
    }

    .simple-table-divider-line {
      height: .5px; 
      padding: 0px; 
      background-color: var(--light-gray);
    }

    .simple-table-header-row {
      border-top: 1px solid var(--light-gray);
      border-right: 1px solid var(--light-gray);
      border-left: 1px solid var(--light-gray);
      background: var(--white);
    }

    .simple-table-row:hover {
      background: var(--lighter-gray);
    }
     
    input:-webkit-autofill,
    input:-webkit-autofill:hover, 
    input:-webkit-autofill:focus, 
    input:-webkit-autofill:active{
        -webkit-box-shadow: 0 0 0 30px var(--white) inset !important;
    }

    .simple-hses-table-container {
      width: 95%;
      max-height: 250px;
      overflow-y: auto;
      border: 1px solid lightgray;
    }

      input[type="checkbox"] {
        width: 1.25em;
        height: 1.25em;
        accent-color: #4A90E2;
        vertical-align: sub;
      }
  </style>
`