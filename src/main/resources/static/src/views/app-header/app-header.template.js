import { html } from 'lit'; 
import { appHeaderStyles } from './app-header.styles.js';
import { sharedStyles } from '../shared/shared-styles.js';

export const appHeaderTemplate = (context) => {
  return html`
    ${ sharedStyles }
    ${ appHeaderStyles }
    <div class="header">
      <menu-view class="menu-view" .labels=${context.labels}></menu-view>
      <div class="logo-title-container">
        <img id="logo1" class="logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII="/>        
        <div clas="title">Purchase Order App</div>
      </div>
      <div class="header-title-container">
        James Tool's Limited
      </div>
       
    </div>
`};
