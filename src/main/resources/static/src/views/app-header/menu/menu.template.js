import { html } from 'lit'; 
import { menuStyles } from './menu.styles.js';
import { sharedStyles } from '../../shared/shared-styles.js';

export const menuTemplate = (context) => {
return html`
  ${sharedStyles}
  ${menuStyles}
  <div id="sidenav" class="sidenav">

    <a href="javascript:void(0)" class="closebtn" @click="${context.closeNav}">&times;</a>
      ${context.navOptions.map((option) => {
        return html`
          <a class="navOptions" id="${option.code}"  @click="${(e) => context.navigate(option.code)}"> ${option.name} </a>
        `
      })}
  </div>
 
  <div class="menu-button">
    <span @click="${ context.openNav }" class="navOptions">&#9776;</span>
  </div>
`};