import { html } from 'lit'; 

export const menuStyles = html`
<style>
  .menu-button {
    text-align: center;
    color: var(--white-gray);
    font-size: 20px;
    background-color: #5465BB;
    height: 20px;
    padding: 30px;
  }

  .sidenav {
    font-family: 'OpenSansLight', Arial, sans-serif;
    height: 100%;
    width: 0;
    position: fixed;
    /* Header z-index is 1, side-menu must overlay it */
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #5465BB;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 100px;
  }

  .navOptions, .closebtn {
    color: var(--white-gray);
  }

  .navOptions:hover {
    cursor:pointer;
  }


  .sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 17px;
    display: block;
    transition: 0.3s;
    white-space: nowrap;
  }

  .sidenav .closebtn {
    position: absolute;
    top: 20px;
    right: 25px;
    font-size: 36px;
    padding-left: 50px;
  }

  @media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
  }

  hr {
    margin: 20px 30px;
    padding: 0px;
  }

</style>
`