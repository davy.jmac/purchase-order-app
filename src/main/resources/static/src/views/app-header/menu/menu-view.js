
import { menuTemplate } from './menu.template.js';
import { LitElement } from "lit";
import { BaseView } from '../../base-view.js';
import { connect } from 'pwa-helpers';
import { store } from '../../../redux/store.js';
import { navigate } from '../../../redux/actions/nav-actions.js';

class MenuView extends connect(store)(LitElement, BaseView) {
  render() {
    return menuTemplate(this);
  }

  static get properties() {
    return {
      navOptions: { type: Array }
    };
  }

  constructor() {
    super();
    this.navOptions = [
      {
        "code" : "catalog",
        "name" : "Parts Catalog"
      },
      {
        "code" : "purchase-orders",
        "name" : "Purchase Orders"
      },
      {
        "code" : "create-purchase-order",
        "name" : "Create Purchase Order"
      }
    ]
  }

  navigate(view) {
    this.closeNav();
    store.dispatch(navigate(view, true))
  }

  openNav() {
    this.shadowRoot.getElementById("sidenav").style.width = "300px";
    this.shadowRoot.addEventListener("click", this.clickListener.bind(this));
  }

  closeNav() {
    this.shadowRoot.getElementById("sidenav").style.width = "0";
    this.shadowRoot.removeEventListener("click", this.clickListener.bind(this));
    this.showVersion = false;
  }

  clickListener(e) {
    if ( !(e.target.id === "sidenav" || e.target.className === "navOptions" || e.target.className === "closeBtn")) {
      this.closeNav();
    }
  }

  stateChanged(state) {
    // console.log(state)
  }
}
  
  customElements.define('menu-view', MenuView); 
