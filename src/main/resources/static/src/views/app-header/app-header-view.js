import { LitElement } from "lit";
import { appHeaderTemplate } from "./app-header.template.js";


class AppHeaderView extends LitElement {
  render() {
    return appHeaderTemplate(this);
  }

  static get properties() {
    return {
      id: { type: String },
      patient: { type: Object },
      labesl: { type: Array },
      hse: { type: Object },
      view: { type: String },
      productName: { type: String },
      flows: { type: Object }
    };
  }

  constructor() {
    super();
    this.id = "header-view";
  }
}

customElements.define("app-header-view", AppHeaderView);
