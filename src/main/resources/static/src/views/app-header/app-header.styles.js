import { html } from "lit";

export const appHeaderStyles = html`
  <style>
    .logo {
      width: 210px;
      height: auto;
      margin-top: 10px;
    }

    .title {
      margin-bottom: 11px;
    }

    .header {
      display: flex;
      align-items: center;
      max-height: 8vh;
      position: sticky;
      top: 0;
      background-color: #3a4ba7;
      width: 100%;
      overflow-x: auto;
    }

    .logo-title-container {
      margin-left: 10px;
      height: 100%;
      display: flex;
      justify-content: center;
      flex-direction: column;
      width: 30%;
      margin-right: 40px;
    }
    
    .patient-container {
      display: flex;
      flex-direction: row;
      width: 100%;
      justify-content: space-between;
    }

    .name-container {
      display: flex;
      flex-direction: column;
      margin-left: 50px;
      bottom: 30px;
      width: 30%;
    }

    .details.container {
      padding-right: 5px;
    }

    .name-label, .referral-type-label {
      font-size: 14px;
      color: var(--lighter-gray)
    }

    .name, .referral-type {na
      font-size: 16px;
      font-weight: normal;
    }

    .details-row {
      display: flex;
      flex-direction: row;
      justify-content: right;
    }
   
    .details-cell {
      margin-right: 40px;
    }

    .details-row-label {
      font-size: 11px;
      color: var(--lighter-gray);
    }

    .details-row-content {
      font-size: 13px;
    }

    .header-title-container {
      font-size: 26px;
    }

    /* Styles for the phone and tablet layout */
    @media only screen and (max-width: 1189px) {
    
    }
  </style>
`;