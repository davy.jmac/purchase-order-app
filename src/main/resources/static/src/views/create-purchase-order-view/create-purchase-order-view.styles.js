import { html } from "lit";

export const createPurchaseOrderViewStyles = html`
  <style>
  
    .layout {
      width: 100%;
    }

    .header-container {
      font-size: 30px;
      padding: 30px;
    }

    .top-bar-container {
      display: flex;
      flex-direction: row;
      width: 100%;
      justify-content: space-between;
      margin-bottom: 20px;
      margin-top: 20px;
      background-color: var(--white-gray);
      align-items: center;
    }

    .parts-header-title-container {

    }

    .inputs-container {
      display: flex;
      flex-direction: row;
    }

    .left-inputs-container {
      display: flex;
      flex-direction: column;
      padding-right: 30px;
      padding-left: 30px;
      width: 20%
    }

    .middle-inputs-container {
      display: flex;
      flex-direction: column;
      padding-right: 30px;
      width: 20%

    } 

    .right-inputs-container {
      display: flex;
      flex-direction: column;
      width: 20%

    } 

    .component-container {
      padding-bottom: 30px;
    }

    .parts-header-container {
      display: flex;
      flex-direction: row;
      width: 100%;
      background-color: var(--white-gray);
    }

    .parts-rows-container {
      display: flex;
      flex-direction: row;
      width: 100%;
    }

    .part {
      display: flex;
      justify-content: center;
      width: 20%;
      border: 2px solid var(--white-gray);
      border-top: none;
    }

    .part-header {
      display: flex;
      justify-content: center;
      width: 20%;
      font-weight: bold;
    }

    .create-purchase-order-button {
      padding-top: 20px;
    }
  </style>
`;