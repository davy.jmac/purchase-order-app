import { html } from 'lit'; 
import { createPurchaseOrderViewStyles } from './create-purchase-order-view.styles.js';
import { sharedStyles } from '../shared/shared-styles.js';

import { InputString } from "../../components/input-string/input-string.js";
import { InputRadioArray } from "../../components/input-radio-array/input-radio-array.js";
import { InputDropdown } from "../../components/input-dropdown/input-dropdown.js";
import { pencilIcon } from "../../icons.js"

export const createPurchaseOrderViewTemplate = (context) => {
  return html`
    ${ sharedStyles }
    ${ createPurchaseOrderViewStyles }
    <div class="wrapper">
      <div class="layout">
        <div class="header-container">
          Create Purchase Orders  
        </div>
        <div class="top-bar-container">
          <div>
            <button
              ?disabled=${context.pageOne}
              @click=${(e) => context.setPage(e)}
            >
              Previous
            </button>
          </div>
          

          <div>
            <button
              ?disabled=${!context.pageOne}
              @click=${(e) => context.setPage(e)}
            >
              Next
            </button>
          </div>
        </div>
        <div class="body-container">
          ${context.pageOne ? 
            html`
              <div class="page-one-container">
                ${buildCustomerInputs(context)}
              </div>
              <div class="error-container">
                ${context.error}
              </div>
            `
          :
            html`
              <div class="page-two-container">
                ${buildPurchaseOrderInputs(context)}
              </div>
              <div class="error-container">
                ${context.error}
              </div>
              <div class="parts-container">
                <div class="parts-header-container">
                  <div class="part-header">
                    Part Number
                  </div>
                  <div class="part-header">
                    Part Name
                  </div>
                  <div class="part-header">
                    Part Quantity
                  </div>
                  <div class="part-header">
                    Part Price
                  </div>
                  <div class="part-header">
                    Part Total
                  </div>
                  <div class="part-header">
                  </div>
                </div>
                ${context.newPurchaseOrder.parts.length > 0 ? context.newPurchaseOrder.parts.map((part, index) => {
                  return html`
                  <div class="parts-rows-container">
                    <div class="part">
                      ${part.partNumber}
                    </div>
                    <div class="part">
                      ${part.partName}
                    </div>
                    <div class="part">
                      ${part.partQuantity}
                    </div>
                    <div class="part">
                      ${part.partPrice}
                    </div>
                    <div class="part">
                      ${part.partTotal}
                    </div>
                    <div class="part"
                     @click=${(e) => context.editPart(e, index)}
                    > 
                      ${pencilIcon}
                    </div>
                  </div>
                  `
                })
                :
                  html``
                }
              </div>
            `
          }

          ${!context.pageOne ? 
            html`
              <div class="create-purchase-order-button">
                <button
                 ?disabled=${!context.submitEnabled}
                 @click=${(e) => context.createPurchaseOrder(e)}
                >
                  Create purchase orders
                </button>
              </div>
            `
          :
            html`

            `
          }
        </div>
      </div>
    </div>
  `
};

function buildCustomerInputs(context) {
  return html`
    <div class="inputs-container">

      <div class="left-inputs-container">
        <div class="component-container">
          <input-string
            id="name"
            name=Name
            .value=${context.newPurchaseOrder.customer.name}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "name"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="address"
            name="Address"
            .value=${context.newPurchaseOrder.customer.address}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "address"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="postalCode"
            name="Postal Code"
            .value=${context.newPurchaseOrder.customer.postalCode}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "postalCode"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-radio-array
            id="pickup"
            name="Pick Up"
            .options=${[{"optionCode" : "true", "description" : "Yes"}, {"optionCode" : "false", "description" : "No"}]}
            ?required=${true}
            .value=${String(context.newPurchaseOrder.customer.pickup)}
            componentWidth=175
            ?readOnly=${(context.invalid !== "" && context.invalid !== "pickup") || context.newPurchaseOrder.customer.shipping}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-dropdown-")) : ""}
          >
          </input-radio-array>
        </div>
      </div>


      <div class="middle-inputs-container">
        <div class="component-container">
          <input-string
            id="email"
            name=Email
            .value=${context.newPurchaseOrder.customer.email}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "email"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="shippingPostalCode"
            name="Shipping Postal Code"
            .value=${context.newPurchaseOrder.customer.shippingPostalCode}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "shippingPostalCode"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-radio-array
            id="shipping"
            name="Shipping"
            .options=${[{"optionCode" : "true", "description" : "Yes"}, {"optionCode" : "false", "description" : "No"}]}
            ?required=${true}
            componentWidth=175
            .value=${String(context.newPurchaseOrder.customer.shipping)}
            ?readOnly=${(context.invalid !== "" && context.invalid !== "shipping") || context.newPurchaseOrder.customer.pickup}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-dropdown-")) : ""}
          >
          </input-radio-array>
        </div>
      </div>
        
      <div class="right-inputs-container">
        <div class="component-container">
          <input-string
            id="phone"
            name="Phone"
            .value=${context.newPurchaseOrder.customer.phone}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "phone"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="shippingAddress"
            name="Shipping Address"
            .value=${context.newPurchaseOrder.customer.shippingAddress}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "shippingAddress"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="company"
            name="Company"
            .value=${context.newPurchaseOrder.customer.company}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "company"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>


      </div>
    </div>
  `;
}


function buildPurchaseOrderInputs(context) {
  return html`
    <div class="inputs-container">
      <div class="left-inputs-container">
      <div class="component-container">
          <input-dropdown
           id="partNumber"
           name="Part Number"
           .options=${[{"optionCode" : "1", "description" : "1"}, {"optionCode" : "2", "description" : "2"}, {"optionCode" : "3", "description" : "3"}, {"optionCode" : "4", "description" : "4"}, {"optionCode" : "5", "description" : "5"}, {"optionCode" : "6", "description" : "6"}, {"optionCode" : "7", "description" : "7"}, {"optionCode" : "8", "description" : "8"}]}
           .value=${context.part.partNumber}
           ?required=${true}
           componentWidth=175
           ?readOnly=${context.invalid !== "" && context.invalid !== "partNumber"}
           .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-droddown-")) : ""}
          >
          </input-dropdown>
        </div>

        <div class="component-container">
          <input-string
            id="partPrice"
            name="Part Price"
            .value=${context.part.partPrice}
            ?required=${true}
            componentWidth=175
            ?readOnly=${true}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>
      </div>

      <div class="middle-inputs-container">
        <div class="component-container">
          <input-string
            id="partName"
            name="Part Name"
            .value=${context.part.partName}
            ?required=${true}
            componentWidth=175
            ?readOnly=${true}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>

        <div class="component-container">
          <input-string
            id="partTotal"
            name="Part Total"
            .value=${context.part.partTotal}
            ?required=${true}
            componentWidth=175
            ?readOnly=${true}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-string>
        </div>
      </div>

      <div class="right-inputs-container">
        <div class="component-container">
          <input-dropdown
            id="partQuantity"
            name="Part Quantity"
            .options=${[{"optionCode" : "1", "description" : "1"}, {"optionCode" : "2", "description" : "2"}, {"optionCode" : "3", "description" : "3"}, {"optionCode" : "4", "description" : "4"}, {"optionCode" : "5", "description" : "5"}, {"optionCode" : "6", "description" : "6"}, {"optionCode" : "7", "description" : "7"}, {"optionCode" : "8", "description" : "8"}, {"optionCode" : "9", "description" : "9"}, {"optionCode" : "10", "description" : "10"}, {"optionCode" : "11", "description" : "11"}, {"optionCode" : "12", "description" : "12"}, {"optionCode" : "13", "description" : "13"}, {"optionCode" : "14", "description" : "14"}, {"optionCode" : "15", "description" : "15"}]}
            .value=${context.part.partQuantity}
            ?required=${true}
            componentWidth=175
            ?readOnly=${context.invalid !== "" && context.invalid !== "partQuantity"}
            .labels=${context.labels ? context.labels.filter((l) => String(l.code).startsWith("input-string-")) : ""}
          >
          </input-dropdown>
        </div>

        <div class="component-container">
          ${context.partIndex === null ? 
            html`
              <button
                @click=${(e) => context.addPart(e)}
              >
                Add Part
              </button>
            `
          :
            html`
              <button
                @click=${(e) => context.savePart(e)}
              >
                Save
              </button>
            `
          }
        </div>
      </div>
  `;
}