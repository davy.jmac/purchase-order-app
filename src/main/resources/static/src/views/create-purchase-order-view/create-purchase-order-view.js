import { LitElement } from "lit";
import { createPurchaseOrderViewTemplate } from "./create-purchase-order-view.template.js";
import { addPurchaseOrder, getPurchaseOrders } from "../../redux/actions/app-actions.js";
import { store } from '../../redux/store.js';
import { connect } from 'pwa-helpers';
import { BaseView } from '../base-view.js';
import  labels  from "../../labels.json"

const SMS_VALIDATION = /\+?1?\s*\(?-*\.*(\d{3})\)?\.*-*\s*(\d{3})\.*-*\s*(\d{4})$/;
const EMAIL_VALIDATION = /^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;

class CreatePurchaseOrderView extends connect(store)(LitElement,BaseView) {
  render() {
    return createPurchaseOrderViewTemplate(this);
  }

  static get properties() {
    return {
      id: { type: String },
      labels: { type: Array },
      pageOne: { type: Boolean },
      newPurchaseOrder: { type: Object },
      part: { type: Object },
      error: { type: String },
      invalid: { type: Object },
      submitEnabled: { type: Boolean },
      parts: { type: Boolean },
      partIndex: { type: Number },
      editMode: { type: Boolean }
    };
  }

  constructor() {
    super();
    this.id = 0;
    this.labels = [];
    this.pageOne = true;
    this.partIndex = null;
    this.editMode = false;
    this.newPurchaseOrder = {
        "id":"",
        "customer" : {
            "name":"",
            "email" : "",
            "phone" : "",
            "address" : "",
            "postalCode" : "",
            "company" : "",
            "shipping" : true,
            "shippingAddress" : "",
            "shippingPostalCode" : "",
            "pickup" : false
        },
        "parts" : []
    };
    this.part = {
        "partNumber" : "",
        "partName" : "",
        "partQuantity" : 0,
        "partPrice" : 0,
        "partTotal" : 0
    }
    this.error = "";
    this.invalid = "";
    this.submitEnabled = false;
    this.parts = [];
    store.dispatch(getPurchaseOrders());
    this.addEventListener("changed-value", this.valueUpdated);
  }

  stateChanged(state) {
    // console.log(state)
    this.id = state.app.purchaseOrders.length + 1;
    // console.log(this.id)

    if (state.app.parts) {
      this.parts = state.app.parts;
    }
  }
  
  valueUpdated(e) {
    this.validate(e);

    switch (e.detail.id) {
      case "name":
        this.newPurchaseOrder.customer.name = e.detail.value;
        break;
      case "email":
        this.newPurchaseOrder.customer.email = e.detail.value;
        break;
      case "address":
        this.newPurchaseOrder.customer.address = e.detail.value;
        break;
      case "postalCode":
        this.newPurchaseOrder.customer.postalCode = e.detail.value;
        break;
      case "phone":
        this.newPurchaseOrder.customer.phone = e.detail.value;
        break;
      case "company":
        this.newPurchaseOrder.customer.company = e.detail.value;
        break;
      case "shipping":
        if (e.detail.value === "true") {
          this.newPurchaseOrder.customer.shipping = true;
        } else {
          this.newPurchaseOrder.customer.shipping = false;
        }
        break;
      case "shippingAddress":
        this.newPurchaseOrder.customer.shippingAddress = e.detail.value;
        break;
      case "shippingPostalCode":
        this.newPurchaseOrder.customer.shippingPostalCode = e.detail.value;
        break;
      case "pickup":
        if (e.detail.value === "true") {
          this.newPurchaseOrder.customer.pickup = true;
        } else {
          this.newPurchaseOrder.customer.pickup = false;
        }
        break;
      case "partNumber":

        if (this.parts.filter((part) => part.partNumber === Number(e.detail.value)).length > 0) {
          let part = this.parts.filter((part) => part.partNumber === Number(e.detail.value))[0]

          this.part.partNumber = part.partNumber;
          this.part.partName = part.partName;
          this.part.partPrice = part.partPrice;
          this.part.partTotal = this.part.partQuantity * this.part.partPrice;
        }

        break;
      case "partPrice":
        break;
      case "partName":
        break;
      case "partQuantity":
        this.part.partQuantity = e.detail.value;
        this.part.partTotal = this.part.partQuantity * this.part.partPrice;
        break;
      default: 
        console.log("Invalid id")
    }
    
    this.newPurchaseOrder = {...this.newPurchaseOrder}
  }

  addPart() {

    if (Object.keys(this.part).length === 5 && this.part.partNumber !== "" 
      && this.part.partPrice !== "" && this.part.partName !== "" 
      && this.part.partTotal !== "" && this.part.partQuantity !== "") {

        if (this.partIndex === null) {
          this.newPurchaseOrder.parts.push(this.part);
        } else {
          this.newPurchaseOrder.parts[index] = this.part;
        }
      
      this.part = {
        "partNumber" : "",
        "partName" : "",
        "partQuantity" : 0,
        "partPrice" : 0,
        "partTotal" : 0
      }
      this.error = "";
      this.submitEnabled = true;
    } else {
      this.error = "Missing required field"
    }
  }

  validate(e) {

    if (e.detail.value !== "") {
      this.invalid = "";
      this.error = "";
    } else {
      this.invalid = e.detail.id;
      this.error = "Field required";
    }

    switch (e.detail.id) {
      case "email":
        if (EMAIL_VALIDATION.test(e.detail.value)) {
          this.invalid = "";
        } else {
          this.invalid = e.detail.id;
          this.error = "Invalid email";
        }
        break;
      case "phone":
        if (SMS_VALIDATION.test(e.detail.value)) {
          this.invalid = "";
        } else {
          this.invalid = e.detail.id;
          this.error = "Invalid phone number";
        }
        break;
      default: 
        break;
    }
  }

  /**
   * finish validation
   * 
   * make readonly when not invalid input
   * 
   * disable submit or screen anv till all fields fileld out 
   */

  setPage() {
    this.pageOne = !this.pageOne;
    this.partIndex = null;
  }


  createPurchaseOrder() {

    if (Object.keys(this.newPurchaseOrder.customer).length === 10
      && this.newPurchaseOrder.customer.name !== "" 
      && this.newPurchaseOrder.customer.email !== "" 
      && this.newPurchaseOrder.customer.phone !== "" 
      && this.newPurchaseOrder.customer.address !== "" 
      && this.newPurchaseOrder.customer.postalCode !== ""
      && this.newPurchaseOrder.customer.company !== ""
      && this.newPurchaseOrder.customer.shippingAddress !== ""
      && this.newPurchaseOrder.customer.shippingPostalCode !== ""
      && this.newPurchaseOrder.parts.length > 0) {

      this.error = "";
      this.submitEnabled = true;


      this.newPurchaseOrder.id = this.id;
      store.dispatch(addPurchaseOrder(this.newPurchaseOrder));
    
      this.pageOne = true;
      this.newPurchaseOrder = {
          "id":"",
          "customer" : {
              "name":"",
              "email" : "",
              "phone" : "",
              "address" : "",
              "postalCode" : "",
              "company" : "",
              "shipping" : true,
              "shippingAddress" : "",
              "shippingPostalCode" : "",
              "pickup" : false
          },
          "parts" : []
      };
      this.part = {
          "partNumber" : "",
          "partName" : "",
          "partQuantity" : 0,
          "partPrice" : 0,
          "partTotal" : 0
      }

    } else {
      this.error = "Missing required field"
      this.submitEnabled = false;
    }
  }

  /**
   * customer needs same bits
   */
  editPart(e, index) {
    this.part = this.newPurchaseOrder.parts[index];
    this.partIndex = index;
  }

  savePart() {
    this.part = {
      "partNumber" : "",
      "partName" : "",
      "partQuantity" : 0,
      "partPrice" : 0,
      "partTotal" : 0
    }
    this.partIndex = null;
  }

}

customElements.define("create-purchase-order-view", CreatePurchaseOrderView);
