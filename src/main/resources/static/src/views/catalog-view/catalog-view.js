import { LitElement } from "lit";
import { catalogViewTemplate } from "./catalog-view.template.js";
import { connect } from 'pwa-helpers';
import { store } from '../../redux/store.js';
import { BaseView } from '../base-view.js';
import { getParts } from "../../redux/actions/app-actions.js";

class CatalogView extends connect(store)(LitElement,BaseView) {
  render() {
    return catalogViewTemplate(this);
  }

  static get properties() {
    return {
      id: { type: String },
      parts: { type: Array }
    };
  }

  constructor() {
    super();
    this.id = "header-view";
    this.parts = [];
    store.dispatch(getParts())
  }

  stateChanged(state) {
    if (state.app.parts) {
      this.parts = state.app.parts;
    }
    console.log(state)
  }


}

customElements.define("catalog-view", CatalogView);
