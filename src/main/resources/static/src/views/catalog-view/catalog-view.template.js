import { html } from 'lit'; 
import { catalogViewStyles } from './catalog-view.styles.js';
import { sharedStyles } from '../shared/shared-styles.js';

export const catalogViewTemplate = (context) => {
  return html`
    ${ sharedStyles }
    ${ catalogViewStyles }
    <div class="header-container">
      Parts Catalog
    </div>
    <div class="parts-container">

      <div class="part-header">

        <div class="part-item">
          Part Number
        </div>

        <div class="part-item">
          Part Name
        </div>

        <div class="part-item">
          Part Price
        </div>

      </div>
      ${context.parts ? context.parts.map((part) => {
          return html`
            <div class="part-row">

              <div class="part-item">
                ${part.partNumber}
              </div>

              <div class="part-item">
                ${part.partName}
              </div>

              <div class="part-item">
                ${part.partPrice}
              </div>

            </div>
          `})
        : 
          html``
      }
    </div>
`};
