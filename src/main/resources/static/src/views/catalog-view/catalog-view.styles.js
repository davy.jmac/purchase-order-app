import { html } from "lit";

export const catalogViewStyles = html`
  <style>
    .header-container {
      font-size: 30px;
      padding: 30px;
    }

    .parts-container {
      display: flex; 
      flex-direction: column;
      padding-left: 30px;
    }

    .part-row {
      display: flex; 
      flex-direction: row;
      width: 80%;
      gap: 100px;
      border: 2px solid var(--white-gray);
    }

    .part-header {
      display: flex; 
      flex-direction: row;
      width: 80%;
      gap: 100px;
      font-weight: bold;
      padding-bottom: 10px;
      border: 2px solid var(--white-gray);
      background-color: var(--white-gray)
    }

    .part-item {
      width: 500px;
    }
  </style>
`;