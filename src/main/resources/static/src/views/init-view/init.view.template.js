import { html } from "lit";
import { initViewStyles } from "./init.view.styles.js";

export const InitViewTemplate = (context) => {
  return html`
  ${initViewStyles}
  `;
};

