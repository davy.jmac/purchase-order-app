import { LitElement } from "lit";
import { InitViewTemplate } from "./init.view.template.js";

import Testlabels from '../../labels.json';

class InitView extends LitElement {
  
  render() {
    return InitViewTemplate(this);
  }

  static get properties() {
    return {
      labels: { type: Array },
      name: { type: String },
      greeting: { type: String }
    };
  }

  constructor() {
    super();
    this.labels = Testlabels.values;
    this.addEventListener("changed-value", this.valueUpdated);

    try {
      this.loadAppComponents();

    } catch (error) {

      console.error("Error Initializing app: ", error);
    }
  }

  loadAppComponents() {
    this.loadAndReloadComponent('header', "app-header-view");
    this.loadAndReloadComponent('main', "router-outlet");
  }

  loadComponentOnce(parent, name) {
    let component = document.getElementById(name);
    
    if (component === null) {
      component = document.createElement(name);
      component.setAttribute("id", name);
      document.getElementsByTagName(parent)[0].appendChild(component);
    }
  }

  loadAndReloadComponent(parent, name) {
    let component = document.getElementById(name);
    
    if (component !== null) {
      component.remove();
    }

    component = document.createElement(name);
    component.setAttribute("id", name);
    document.getElementsByTagName(parent)[0].appendChild(component);
  }

}

customElements.define("init-view", InitView);
