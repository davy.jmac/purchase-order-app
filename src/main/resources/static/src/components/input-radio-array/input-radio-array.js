import { LitElement } from "lit";
import { InputRadioArrayTemplate } from "./input-radio-array.template";

class InputRadioArray extends LitElement {
  render() {
    return InputRadioArrayTemplate(this);
  }

  static get properties() {
    return {
      value: { type: String },
      options: { type: Array},
      labels: { type: Array }, 
      ids: { type: Array },
      invalid: { type: Boolean },
      readOnly: { type: Boolean },
      required: { type: Boolean },
      labelWidth: { type: Number },
      id: { type: String },
      name: { type: String },
      orientation: { type: String }
    };
  }

  constructor() {
    super();
    this.id = "";
    this.name = "";
    this.orientation = "horizontal";
    this.value = null;
    this.options = [];
    this.labels = [];
    this.ids = [];
    this.labelWidth = 0;
    this.readOnly = false;
    this.required = false;
    this.invalid = false;
  }

  updated(changedProperties) {
    
    if (changedProperties.has("value") && changedProperties.get("value")) {
      this.shadowRoot.getElementById(changedProperties.get("value")).checked = false;
    }
    
  }
  
  connectedCallback() {
    super.connectedCallback();
    if (!this.labelWidth) {
      this.labelWidth = 0;
    } 
  }

  updateValue(code) {
    this.value = code;
    this.validate();

    let changedValueEvent = new CustomEvent('changed-value', {
      detail: { id: this.id,
                value: this.value,
                type: 'radio',
                invalid: this.invalid  },
      bubbles: true, 
      composed: true });
    this.dispatchEvent(changedValueEvent);
  }

  validate() {
    if (this.required && this.value === null) {
      this.invalid = true;
    } else {
      this.invalid = false;
    }
  }

}

customElements.define("input-radio-array", InputRadioArray);