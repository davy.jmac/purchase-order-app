import { html } from "lit";
import { radioArrayStyles } from "./input-radio-array.style";
import { sharedStyles } from "../../views/shared/shared-styles.js";

export const InputRadioArrayTemplate = (context) => {
    return html`
        ${radioArrayStyles(context)}
        ${sharedStyles}
        <div class="wrapper">
            <div class="layout">
                <div class="radio-container orientation-${context.orientation}">
                    <label class="component-label" for="radio-buttons">
                        <span class="asterisk">${context.required ? "*" : ""}</span>${context.name}
                    </label>
                    <div class="radio-buttons pointer orientation-${context.orientation}">
                        ${context.options.map((option) => {

                            return html`
                                <label class="array-input-label">
                                    <input
                                    id="${option.optionCode}"
                                    type="radio"
                                    name="radio-array"
                                    ?checked=${context.value === option.optionCode}
                                    ?disabled=${context.readOnly}
                                    @change=${(e) => context.updateValue(option.optionCode)}
                                    >
                                    ${option.description}
                                </label>
                            `;
                        })}
                    </div>
                </div>
            </div>
        </div>
    `
};