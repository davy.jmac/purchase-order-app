import { html } from "lit";

export const radioArrayStyles = (context) =>{
    return html`
    <style>
        .layout {
            display: flex;
            flex-direction: column;
            margin-top: 15px; 
        }

        input[type="radio"] {
            width: 1.25em;
            height: 1.25em;
            accent-color: #4A90E2;
            vertical-align: sub;
        }

        .radio-buttons {
            /* flex-wrap: wrap; */
            width: max-content;
        } 

        .component-label {
            display: flex;
            flex-direction: row;
            min-width: ${context.labelWidth}px;
        }

        .popup {
            position: relative;
            top: 0;
            right: 0;
        }
    </style>
`;
}