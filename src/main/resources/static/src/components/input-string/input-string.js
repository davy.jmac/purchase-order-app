import { LitElement } from "lit";
import { InputStringTemplate } from "./input-string.template";

class InputString extends LitElement {
  render() {    
    return InputStringTemplate(this);
  }

  static get properties() {
    return {
      id: { type: String },
      name: { type: String },
      maxLength: { type: Number },
      minLength: { type: Number },
      size: { type: Number },
      validationExp: { type: Object },
      error: { type: String },
      value: { type: String },
      required: { type: Boolean },
      readOnly: { type: Boolean },
      invalid: { type: Boolean },
      customStyles: { type: String },
      labels: { type: Array },
      labelWidth: { type: Number } ,
      prefix: { type: String },
      componentWidth: { type: Number },
      borderColour: { type: String },
      jumpingLabelEnabled: { type: Boolean }
    };
  }

  constructor() {
    super();
    this.id = "stringField";
    this.name = "";
    this.value = "";
    this.maxLength = 140;
    this.minLength = 0;
    this.required = false;
    this.invalid = false;
    this.readOnly = false;
    this.customStyles = false;
    this.validationExp = /^[^\p{Cc}]*$/u;
    this.error = "";
    this.labels = [];
    this.labelWidth = 0;
    this.componentWidth = 200;
    this.borderColour = "";
    this.jumpingLabelEnabled = null;
  }

  shortcutListener(e) {
    if (e.key === 'Enter') { 
      this.updateValue();
    }
  }

  connectedCallback() {
    super.connectedCallback();
    if (!CSS.supports('color',this.borderColour)) {
      this.borderColour = "var(--black)"
    }

    if (this.jumpingLabelEnabled === null || this.jumpingLabelEnabled === undefined) {
      this.jumpingLabelEnabled = true;
    }

    if (!this.componentWidth) {
      this.componentWidth=143;
    }
  }

  updateValue(e) {
    this.validate();
    if (this.prefix) {
      this.shadowRoot.getElementById("prefix").style.outline = "none";
    }

    this.value = this.shadowRoot.getElementById(this.id).value.trim();

    let changedValueEvent = new CustomEvent('changed-value', { 
      detail: { id: this.id,
                value: this.value,
                type: 'string',
                invalid: this.invalid },
      bubbles: true, 
      composed: true });
    this.dispatchEvent(changedValueEvent);
  }

  focusElement(e) {
    if (this.prefix) {
      this.shadowRoot.getElementById("prefix").style.outline = "-webkit-focus-ring-color auto 1px";
    }
  }

  validate() {
    let str = this.shadowRoot.getElementById(this.id).value.trim();
    
    // if (this.labels.length === 0) {
    //   this.invalid = false;
    //   this.error = "";
    //   return;
    // }
    
    // if(!this.required && str === "") {
    //   this.invalid = false;
    //   this.error = "";
    // } 
    
    if (this.required && str && str === "") {
      this.invalid = true;
      this.error = this.name + this.labels.find((l) => l.code === "input-string-required-error").description;
    } else if (str.length > this.maxLength) {
      this.invalid = true;
      this.error = this.labels.find((l) => l.code === "input-string-max-character-error").description +"("+this.maxLength +")";
    } else if (str.length < this.minLength) {
      this.invalid = true;
      this.error = this.labels.find((l) => l.code === "input-string-min-character-error").description +"("+this.minLength +")";
    } else {
        this.error = "";
        this.invalid = false;
    }

    // Regex Validation if input is not blank or invalid
    if (this.validationExp && !this.invalid && str !== "") {
      if (!this.validationExp.test(str)) {
        this.invalid = true;
        this.error = this.labels.find((l) => l.code === "input-string-invalid-error").description;
      }
    }
  }
  
}

customElements.define("input-string", InputString);
