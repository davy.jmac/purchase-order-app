import { html } from "lit";

export const inputStringStyles = (context) => {
  return html `
  <style>
    .layout {
      display: flex;
      flex-direction: column;
    }
    
    .component-label {
      left: 0;
      bottom: 5px;
      overflow-x: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      width: ${context.componentWidth}px;
      padding-right: ${context.componentWidth/8}px;
      overflow: hidden;
      box-sizing: border-box;
    }

    input:disabled {
      background-color: white;
    }

    .jumping-label {
      align-items: bottom;
      pointer-events: none;
      position: absolute;
      transform-origin: 0 50%;
      transition: transform 200ms, color 200ms;
      top: 8px;
    }

    .single-input-container-string input {
      padding-top: 10px;
      min-height: 26px;
      outline: none;
      border: none;
      border-bottom: solid 1.5px ${context.borderColour};
      position: relative;
      width: 100%;
    }

    .input:focus ~ .cut,
    .input:not(:placeholder-shown) ~ .cut {
      transform: translateY(8px);
      text-overflow: ellipsis;
    }

    .input:focus ~ .placeholder,
    .input:not(:placeholder-shown) ~ .placeholder {
      transform: translateY(20px) translateX(10px) scale(0.75);
      text-overflow: ellipsis;
      display: ${context.jumpingLabelEnabled ? "flex" : "none"};
    }

    .single-input-container-string {
      height: 35px;
      display: flex;
      flex-direction: row;
      position: relative;
      width: ${context.componentWidth}px;
      border-bottom: solid 1.5px ${context.borderColour};
    }
  </style>
`;
}

