import { html } from "lit";
import { inputStringStyles } from "./input-string.styles";
import { sharedStyles } from "../../views/shared/shared-styles.js";

export const InputStringTemplate = (context) => {
  return html`
    ${inputStringStyles(context)}
    ${sharedStyles}
    <div class="wrapper ${context.invalid ? "invalid" : ""}">
      <div class="layout"> 
          ${ context.prefix ? 
            html `
              <span id="prefix">
                ${context.prefix}
                <div class="single-input-container-string">
                  <input
                  class="input i-beam"
                  type="text"
                  id=${context.id + "-0"}
                  .value=${context.value}
                  size=${context.size ? context.size : ''}
                  placeholder=" "
                  ?readonly=${context.readOnly}
                  @keyup=${(e) => context.shortcutListener(e)}
                  @input=${context.resizeInput}
                  />
                  <div class="cut"></div>
                  <label for=${context.id} class="jumping-label component-label placeholder">
                    ${context.name}<span class="asterisk"> ${context.required ? "*" : ""} </span>
                    <span class="astrisk">${context.isRequired ? "*" : ""}</span>
                    ${context.name}
                  </label>
                  <cds-output-popup
                   .cdsInformation=${context.cdsInformation}
                  ></cds-output-popup>
                </div>
              </span>
            `:
            html`
              <div class="single-input-container-string">
                <input
                 class="input"
                 type="text"
                 id=${context.id}
                 .value=${context.value}
                 size=${context.size ? context.size : ''}
                 placeholder=" "
                 ?disabled=${context.readOnly}
                 @focusout=${(e) => context.updateValue(e)}
                 @keyup=${(e) => context.shortcutListener(e)}
                 @input=${context.resizeInput}
                />
                <div class="cut"></div>
                <label for=${context.id} class="jumping-label component-label placeholder">
                  <span class="asterisk"> ${context.required ? "*" : ""} </span>
                  ${context.name}
                </label>
                <cds-output-popup
                 .cdsInformation=${context.cdsInformation}
                ></cds-output-popup>
              </div>
            `
          }
        <div class="error-container">
          ${context.error}
        </div>
      </div>
    </div>
  `;
};
