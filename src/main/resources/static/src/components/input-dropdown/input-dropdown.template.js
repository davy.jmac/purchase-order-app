import { html } from 'lit';
import { inputDropdownStyles } from './input-dropdown.styles.js';
import { sharedStyles } from "../../views/shared/shared-styles.js";

export const inputDropdownTemplate = (context) => {
  return html `
  ${inputDropdownStyles(context)}
  ${sharedStyles}
  <div class="wrapper">      
    <div class="layout">
      <div class="dropdown-container">
        <div class="dropdown-head-container" @mouseover=${context.flipDropDown} @click='${context.toggle}'>
          <input type="text" READONLY
            id=${context.id}
            class="input"
            placeholder=" "
            value=${context.value ? context.options.filter((option) => context.value === option.optionCode).map((option) => option.description) == '' ? context.value : context.options.filter((option) => context.value === option.optionCode).map((option) => option.description) : ''}
            ?disabled=${context.readOnly}
          >
          ${context.readOnly ? html `` : html ` <span tabindex="-1" class="arrow ${context.closed ? 'closed' : 'open'}">${context.getIcon("arrow")}</span>`}
          <div class="cut"></div>
          <label class="jumping-label component-label placeholder" for="dropdown-input">
            <span class="asterisk"> ${context.required ? "*" : ""} </span>
            ${context.name}
          </label>
        </div>
        <div class="dropdown-box-container" tabindex="-1">
          <div class="dropdown-options ${context.closed ? 'closed' : 'open'}">
              ${context.options.map((option, index) => html`
                <div
                id=${option.optionCode}
                class="option ${option.optionCode === context.value  ? 'chosen' : 'notChosen'}" 
                @click=${e => context.updateValue(e, option.optionCode)}
                >
                  ${option.description}
                </div>`
              )}
          </div>
        </div>
      </div>
      <div class="error-container">
          ${context.error}
      </div>
    </div>
  </div>
    `
}