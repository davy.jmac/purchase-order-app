import { html } from "lit";

export const inputDropdownIcons = {
  arrow: html` <svg 
    xmlns="http://www.w3.org/2000/svg" 
    width="11.497" 
    height="7.246" 
    viewBox="0 0 11.497 7.246"
  >
    <defs>
      <style>
        .a {
          fill:none;
          stroke:#bdbdbd;
          stroke-miterlimit:10;
          stroke-width:2.118px;
        }
      </style>
    </defs>
    <path 
      class="a" 
      d="M0,0,5,5l5-5" 
      transform="translate(0.749 0.749)"
    />
  </svg>
  `,
};