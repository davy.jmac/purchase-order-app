import { LitElement } from 'lit';
import { inputDropdownTemplate } from './input-dropdown.template.js';
import { inputDropdownIcons } from './icons.js'

class inputDropdown extends LitElement {

  render() {
    return inputDropdownTemplate(this);
  }

  static get properties() {
    return {
      options: { type: Array },
      value: { type: String },
      required: { type: Boolean },
      closed: { type: Boolean },
      invalid: { type: Boolean },
      readOnly: { type: Boolean },
      size: { type: Number },
      id: { type: String },
      name: { type: String },
      error: { type: String }, 
      labelWidth: { type: String },
      componentWidth: { type: Number },
      borderColour: { type: String },
      textAlignment: { type: String},
      jumpingLabelEnabled: { type: Boolean },
      componentHeight: { type: Number },
      labels: { type: Array }
    };
  }

  constructor() {
    super();
    this.options = [];
    this.value = null;
    this.required = false;
    this.closed = true;
    this.invalid = false;
    this.readOnly = false;
    this.labelWidth = "";
    this.size = null;
    this.id = "input-dropdown";
    this.name = "";
    this.error = "";
    this.addEventListener('focusout', this.closeDropdownMenu);
    this.addEventListener('scroll', this.scrollListener);
    this.componentWidth = 200;
    this.componentHeight = 135;
    this.dropdownDirectionOffset = 0;
    this.borderColour = "";
    this.textAlignment = "";
    this.jumpingLabelEnabled = null;
    this.labels = [];
  }

  connectedCallback() {
    super.connectedCallback();
    if (!CSS.supports('color',this.borderColour)) {
      this.borderColour =  "var(--black)";
    }

    if (!this.textAlignment) {
      this.textAlignment = "left";
    }

    if (this.jumpingLabelEnabled === null || this.jumpingLabelEnabled === undefined) {
      this.jumpingLabelEnabled = true;
    }

    if (!this.componentHeight) {
      this.componentHeight = (this.options.length <= 3 ? (this.options.length * 43.75) : 175);
    }
    if (!this.componentWidth) {
      this.componentWidth = 143;
    }
  }

  updateValue(e, code) {
    if (code && code !== '') {
      this.value = code;
    }
    this.validate();

    if (!this.closed) {
      this.close();
    }

    let changedValueEvent = new CustomEvent('changed-value', {
    detail: { id: this.id,
              value: this.value,
              type: 'dropdown',
              invalid: this.invalid },
    bubbles: true, 
    composed: true });
    this.dispatchEvent(changedValueEvent);
  }

  getIcon(iconName) {
    if (iconName === "arrow") {
      return inputDropdownIcons.arrow;
    }
  }

  closeDropdownMenu() {
    if (!this.closed) {
      this.close();
      this.updateValue();
    }
  }

  toggle() {
    if (this.readOnly) {
      return;
    }
    this.closed = !this.closed;
    if (!this.closed) {
      this.shadowRoot.querySelector('.input').focus();
      if (this.dropdownDirectionOffset < 0) {
        this.shadowRoot.querySelector('.input').style.borderBottom = "solid 1.5px #4A90E2";
      } else {
        this.shadowRoot.querySelector('.input').style.borderBottom = "solid 0px #4A90E2";
      }
    } else {
        this.shadowRoot.querySelector('.input').blur();
        this.shadowRoot.querySelector('.input').style.borderBottom = "1.5px solid " + this.borderColour;
    }
  }

  close() {
    this.closed = true;
    this.shadowRoot.querySelector('.input').style.borderBottom = "1.5px solid " + this.borderColour;
  }

  scrollListener(e) {
    e.stopPropagation();
    return;
  }
  
  flipDropDown() {
      let dropDownDiv = this.shadowRoot.querySelector('.dropdown-head-container');
      let bounding = dropDownDiv.getBoundingClientRect();
      if (bounding.top + this.componentHeight > window.innerHeight) {
        this.dropdownDirectionOffset = -(bounding.height + this.componentHeight) - 10;
      } else {
        this.dropdownDirectionOffset = 0;
      }
  }

  validate() {
    if (this.labels.length === 0) {
      this.invalid = false;
      this.error = '';
      return;
    }

    if(!this.required && (this.value === '' || this.value === null)) {
      this.invalid = false;
      this.error = '';
    } else if (this.required && (this.value === '' || this.value === null)) {
      this.invalid = true;
      this.error = `${this.name} ${this.labels.find((l) => l.code === 'input-dropdown-required-error').description}.`;
    } else {
      this.invalid = false;
      this.error = '';
    }
  }
  
}

customElements.define("input-dropdown", inputDropdown);