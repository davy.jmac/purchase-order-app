import { html } from "lit";

export const inputDropdownStyles = (context) => {
  let shortOptionList = context.options.length <= 7;
  let listLength = 35 * context.options.length;
  return html `
  <style>
    .wrapper {
      position: relative;
      display: flex;
      align-items: baseline;
      justify-content: flex-start;
    }
   
    .layout {
      display: flex;
      flex-direction: column;
    }

    input:disabled {
      background-color: white;
    }

    .dropdown-container{
      width: ${context.componentWidth}px;
    }

    #dropdown-input{
      width: 100%; 
      padding-right: ${context.componentWidth/8}px;
      overflow: hidden; 
      background-color: ${context.readOnly ? "var(--white-gray)" : "var(--white)"};
    }

    .input {
      text-align: ${context.textAlignment};
      padding-left: 0;
      overflow: hidden;
    }

    .dropdown-head-container {
      width: ${context.componentWidth}px;
      height: 35px;
      display: flex;
      flex-direction: row;
      position: relative;
      background-color: ${context.readOnly ? "var(--white-gray)" : "var(--white)"};
    }

    .dropdown-head-container input {
      width: 100%;
      padding-top: 10px;
      min-height: 26px;
      outline: none;
      border: none;
      /* replace with a gray */
      border-bottom: 1.5px solid ${context.borderColour};
      position: relative;
    }

    .dropdown-head-container input:focus {
      width: ${context.componentWidth}px;
    }

    .arrow {
      display: flex;
      align-items: center;
      position: absolute;
      height: 20px;
      bottom: 1px;
      right: 5px;
      z-index: 0;
      outline: none;
    }

    .arrow.open {
      transform: rotate(180deg);
    }

    .dropdown-box-container {
      position: relative;
      width: ${context.componentWidth}px;
      outline: none;
    }

    .dropdown-options {
      overflow-y: auto;
      overflow-x: hidden;
      line-height: 1em;
      vertical-align: center;
      font-size: 13.3333px;
      position: absolute;
      height: ${context.componentHeight - 1}px;
      width: 90%;
      z-index: 1;
    }

    .dropdown-options.open {
      background-color: var(--white);
      transform: translateY(${context.dropdownDirectionOffset}px);
      width: ${context.componentWidth}px;
      height: ${shortOptionList ? listLength + "px" : "220px"};
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      border-bottom: solid 1.5px #4A90E2;
      border-top: solid 1.5px #4A90E2;
    }

    .dropdown-options.closed {
      width: ${context.componentWidth}px;
      display: none;
    }

    .option {
      width: 95%;
      padding: 10px;
      overflow-x: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      background-color: var(--white);
      color: black;
    }

    .unselect.noclear {
      display: none;
    }
    
    .option:hover {
      background-color: var(--white-gray);
      cursor: pointer;
    }

    .option.chosen {
      background-color: var(--lighter-gray);
      font-weight: bold;
    }

    .option.chosen:hover {
      background-color: var(--white-gray);
    }

    .component-label {
      left: 0;
      bottom: 5px;
      overflow-x: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      width: ${context.componentWidth}px;
      padding-right: ${context.componentWidth/8}px;
      box-sizing: border-box;
    }

    .jumping-label {
      overflow: hidden;
      align-items: bottom;
      pointer-events: none;
      position: absolute;
      transform-origin: 0 50%;
      transition: transform 200ms, color 200ms;
      top: 8px;
      text-align: ${context.textAlignment};
    }

    .popup {
      position: absolute;
      top: 0;
      right: 0;
    }

    .input:focus ~ .cut,
    .input:not(:placeholder-shown) ~ .cut {
      text-overflow: ellipsis;
      width: ${context.componentWidth+context.componentWidth/8}px;
    }

    .input:focus ~ .placeholder,
    .input:not(:placeholder-shown) ~ .placeholder {
      text-overflow: ellipsis;
      display: ${context.jumpingLabelEnabled ? "flex" : "none"};
      width: ${context.componentWidth+context.componentWidth/8}px;
    }

    ::value {
      margin-left: 0px;
      border-style: solid;
    }

    .cut {
      position: absolute;
      top: -20px;
      transform: translateY(0);
      transition: transform 200ms;
      white-space: normal;
    }

    .layout .error-container {
      top: 1.5px;
      position: relative;
      padding-left: 30px;
    }
  </style>
`;}
