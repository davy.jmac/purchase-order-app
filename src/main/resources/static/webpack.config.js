const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const MomentTimezoneDataPlugin = require('moment-timezone-data-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');


module.exports = {
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html',
      favicon: "./src/assets/img/favicon.ico",
      minify: {
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: true
      }
    }),
    new MomentLocalesPlugin({
      localesToKeep: ['en-ca', 'fr-ca', 'es'],
    }),
    new MomentTimezoneDataPlugin({
      matchCountries: ['US', 'CA', 'GB'],
      startYear: 2020,
      endYear: 20025,
    }) 
  ],
  entry: {
    main: './src/index.js'
  },
  output: {
    filename: '[name].[chunkhash:8].js'
  },
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
      },

      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|jp(e*)g|svg|ico)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
           outputPath: 'img/'
          }
        }]
      },
      // {
      //   test: /\.html$/,
      //   use: [{
      //     loader: 'html-loader',
      //     options: {
      //       attrs: ['img:src', 'link:href']
      //   }
      // }]
      // }
    ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'initial'
        },
      },
    }
  }
};
