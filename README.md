# Purchase Orders App

## Prerequisites
- **Visual Studio Code**: Attached is a link to download visual studio code for your operating system / hardware type. Ref: https://code.visualstudio.com/#alt-downloads
- **Node**: Attached is a link for downloading node.js if it is not already on your computer. An easy way to check is to open a new terminal widnow and run 'node -v'

## Steps to Setup

1. **Download Visual Studio Code**: Visual studio code is an IDE which i used to build this porject. It is required in order to run the backend of the Purchase Order App

2. **Import the App Files**: 
    - Open visual studio code
    - Import the unzipped project files into visual studio code

3. **Run the Applications Frontend**
    - Locate the project files in your finder/file explorer
    - Navigate to the static directory and open this directory in terminal
        - Path: purchase-order-app/src/main/resources/static
    - Run the following commands:
        - npm install (install node package manager for the application)
        - npm run dev (run the application on port 8080 or the next available port)

4. **Run the Applications Backend**
    - Open the backend folder
    - Open the server.js file
    - In visual studio code menu on the left click this icon:
        ![Alt text](image.png)
    - Select run and debug
        ![Alt text](image-1.png)
        - if prompted to select debug profile, choose node
    - This will run the applications backend express.js server on port 3000 
    - There is an endpoint you can hit to verify the server is running, it should simply have text reading 
    'Backend of purchase order app' (http://localhost:3000/)
    - The system will no be accesible from the URL outputted by the npm run dev command in step 3
        - the port should be in the range of 8000 - 8010 depdning on what else you are running on your system.
